/**
 * @author Johan De Claville Christiansen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <stdio.h>
#include <string.h>
#include <conf_a3200.h>

#include <gpio.h>
#include <sysclk.h>
#include <FreeRTOS.h>
#include <task.h>

#include <spi.h>
#include <gpio.h>
#include <board.h>
#include <conf_access.h>
#include <reset_cause.h>

#include <dev/usart.h>
#include <dev/i2c.h>

#include <pwr_switch.h>
#include <lm70.h>
#include <mpu3300.h>
#include <hmc5843.h>
#include <fm33256b.h>
#include <led.h>
#include <adc_channels.h>
#include <spn_fl512s.h>

static void init_spi(void) {
	gpio_map_t spi_piomap = {
		{AVR32_SPI1_SCK_0_1_PIN, AVR32_SPI1_SCK_0_1_FUNCTION},
		{AVR32_SPI1_MOSI_0_1_PIN, AVR32_SPI1_MOSI_0_1_FUNCTION},
		{AVR32_SPI1_MOSI_0_2_PIN, AVR32_SPI1_MOSI_0_2_FUNCTION},
		{AVR32_SPI1_MISO_0_1_PIN, AVR32_SPI1_MISO_0_1_FUNCTION},
		{AVR32_SPI1_NPCS_2_2_PIN, AVR32_SPI1_NPCS_2_2_FUNCTION},
		{AVR32_SPI1_NPCS_3_2_PIN, AVR32_SPI1_NPCS_3_2_FUNCTION},
		#ifdef ENABLE_SPANSION
		{AVR32_SPI1_NPCS_0_PIN, AVR32_SPI1_NPCS_0_FUNCTION},
		{AVR32_SPI1_NPCS_1_2_PIN, AVR32_SPI1_NPCS_1_2_FUNCTION},
		#endif
	};
	#ifdef ENABLE_SPANSION
	gpio_enable_module(spi_piomap, 8);
	#else
	gpio_enable_module(spi_piomap, 6);
	#endif

	/* SPI1 setup */
	sysclk_enable_pba_module(SYSCLK_SPI1);
	spi_reset(&AVR32_SPI1);
	spi_set_master_mode(&AVR32_SPI1);
	spi_disable_modfault(&AVR32_SPI1);
	spi_disable_loopback(&AVR32_SPI1);
	spi_set_chipselect(&AVR32_SPI1, (1 << AVR32_SPI_MR_PCS_SIZE) - 1);
	spi_disable_variable_chipselect(&AVR32_SPI1);
	spi_disable_chipselect_decoding(&AVR32_SPI1);
	spi_enable(&AVR32_SPI1);
}

static void init_spn_fl512(void) {
	spn_fl512s_init((unsigned int) 0);
}

static void init_rtc(void) {

	/* Setup RTC */
	uint8_t cmd[] = {FM33_WRPC, 0x18, 0x3D};
	fm33256b_write(cmd, 3);

	/* RTC */
	fm33256b_clock_resume();

	/* 32kHz Crystal setup */
	osc_enable(OSC_ID_OSC32);

}

static void print_rst_cause(int reset_cause) {
	printf("Reset cause ");
	switch (reset_cause) {
		case RESET_CAUSE_WDT:
			printf("WDT\r\n");
			break;
		case RESET_CAUSE_SOFT:
			printf("SOFT\r\n");
			break;
		case RESET_CAUSE_SLEEP:
			printf("SLEEP\r\n");
			break;
		case RESET_CAUSE_EXTRST:
			printf("EXT RST\r\n");
			break;
		case RESET_CAUSE_CPU_ERROR:
			printf("CPU ERROR\r\n");
			break;
		case RESET_CAUSE_BOD_CPU:
			printf("BOD CPU\r\n");
			break;
		case RESET_CAUSE_POR:
			printf("POR\r\n");
			break;
		case RESET_CAUSE_JTAG:
			printf("JTAG\r\n");
			break;
		case RESET_CAUSE_OCD:
			printf("OCD\r\n");
			break;
		case RESET_CAUSE_BOD_IO:
			printf("BOD IO\r\n");
			break;
		default:
			printf("UNKNOWN\r\n");
	}
}

/* Init I2C */
static void twi_init(void) {
	/* GPIO map setup */
	const gpio_map_t TWIM_GPIO_MAP = {
		{AVR32_TWIMS2_TWCK_0_0_PIN, AVR32_TWIMS2_TWCK_0_0_FUNCTION},
		{AVR32_TWIMS2_TWD_0_0_PIN, AVR32_TWIMS2_TWD_0_0_FUNCTION}
	};
	gpio_enable_module(TWIM_GPIO_MAP,
			sizeof(TWIM_GPIO_MAP) / sizeof(TWIM_GPIO_MAP[0]));

	/* Init twi master controller 2 with addr 5 and 100 kHz clock */
	i2c_init_master(2, 5, 100);
}


void init_task(void * param) {

	/* SPI device drivers */
	init_spi();
	lm70_init();
	fm33256b_init();
	vTaskDelay(100 / portTICK_RATE_MS);
	adc_channels_init();
	vTaskDelay(100 / portTICK_RATE_MS);

	/* Init I2C */
	twi_init();

	/* Latest reset source */
	print_rst_cause(reset_cause_get_causes());
	
	/* RTC + 32kHz OSC */
	init_rtc();

	/* Init spansion chip */
	init_spn_fl512();

	/* Init gyro */
	mpu3300_init(MPU3300_BW_5, MPU3300_FSR_225);
	
	/* Init magnetometer */
	hmc5843_init();

	/* Start led blink task */
	void task_blink(void * param);
	xTaskCreate(task_blink, "BLINK", 1000, NULL, 1, NULL);
	
	/* Start watch dog clear task */
	void task_wdt_clear(void * param);
	xTaskCreate(task_wdt_clear, "WDT", 1000, NULL, 1, NULL);
	
	/* Start sensor sample task */
	void task_sample_sensors(void * param);
	xTaskCreate(task_sample_sensors, "SAMPLE", 1000, NULL, 1, NULL);

	vTaskDelete(NULL);
}
