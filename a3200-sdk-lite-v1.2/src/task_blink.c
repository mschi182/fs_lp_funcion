#include <stdio.h>

#include <conf_a3200.h>

#include <FreeRTOS.h>
#include <task.h>

#include <led.h>
#include <lm70.h>

void task_blink(void * param) {
	while (1) {
		led_off(LED_CPUOK);
		led_on(LED_A);
		vTaskDelay(1000 / portTICK_RATE_MS);
		led_off(LED_A);
		led_on(LED_CPUOK);
		vTaskDelay(1000 / portTICK_RATE_MS);
	}
}
