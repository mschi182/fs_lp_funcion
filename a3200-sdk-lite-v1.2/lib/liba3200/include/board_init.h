/**
 * ADC channel driver
 *
 * @author Johan de Claville Christiansen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#ifndef BOARD_INIT_H_
#define BOARD_INIT_H_

void init_can(int enable_can);
void board_init(void);

#endif /* BOARD_INIT_H_ */
