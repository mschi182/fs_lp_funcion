#ifndef MPU3300
#define MPU3300

#include <stdint.h>

#define MPU3300_I2C_ADDR		0x68

/* Registers */
#define MPUREG_SELF_TEST_X		0x0D
#define MPUREG_SELF_TEST_Y		0x0E
#define MPUREG_SELF_TEST_Z		0x0F
#define MPUREG_SMPLRT_DIV		0x19
#define MPUREG_CONFIG			0x1A
#define MPUREG_GYRO_CONFIG		0x1B
#define MPUREG_TEMP_OUT_H		0x41
#define MPUREG_TEMP_OUT_L		0x42
#define MPUREG_GYRO_XOUT_H		0x43
#define MPUREG_GYRO_XOUT_L		0x44
#define MPUREG_GYRO_YOUT_H		0x45
#define MPUREG_GYRO_YOUT_L		0x46
#define MPUREG_GYRO_ZOUT_H		0x47
#define MPUREG_GYRO_ZOUT_L		0x48
#define MPUREG_SIGNAL_PATH_RESET 	0x68
#define MPUREG_USER_CTRL		0x6A
#define MPUREG_PWR_MGMT_1		0x6B
#define MPUREG_PWR_MGMT_2		0x6C
#define MPUREG_WHO_A_I			0x75


extern int mpu3300_full_scale_reading;

typedef enum mpu3300_full_scale_reading_e {
	MPU3300_FSR_225 = 0,
	MPU3300_FSR_450 = 1,
} mpu3300_full_scale_reading_t;

typedef enum mpu3300_bandwidth_e {
	MPU3300_BW_256 = 0,
	MPU3300_BW_188 = 1,
	MPU3300_BW_98 = 2,
	MPU3300_BW_42 = 3,
	MPU3300_BW_20 = 4,
	MPU3300_BW_10 = 5,
	MPU3300_BW_5 = 6,
} mpu3300_bandwidth_t;

typedef struct mpu3300_gyro_s {
	float gyro_x;
	float gyro_y;
	float gyro_z;
} mpu3300_gyro_t;

typedef struct mpu3300_gyro_raw_s {
	int16_t gyro_x;
	int16_t gyro_y;
	int16_t gyro_z;
} mpu3300_gyro_raw_t;

/**
 * Run gyro self test and return the result. If returning -1
 * the device can be in an unknown state and should be reinit.
 * @return -1 if i2c err, -2 if selftest failed else 0
 */
int mpu3300_selftest(void);
/**
 * Initialize gyro registers.
 * @param bandwidth sets the gyro bandwidth
 * @param full_scale sets full scale reading range
 * @return -1 if i2c err, else 0
 * */
int mpu3300_init(mpu3300_bandwidth_t bandwidth, mpu3300_full_scale_reading_t full_scale);
/**
 * Resets all internal registers to default values.
 * @return -1 if i2c err, else 0
 * */
int mpu3300_reset(void);
/** 
 * Read gyro.
 * @param gyro_readings gyro x,y,z rates as degrees/s
 * @return -1 if i2c err, else 0
 */
int mpu3300_read_gyro(mpu3300_gyro_t * gyro_readings);
/**
 * Read gyro temperature.
 * @param temp the temperature read
 * @return -1 if i2c err, else 0
 */
int mpu3300_read_temp(float * temp);

/**
 * Read gyro and do not scale the result.
 * @param gyro_reading gyro x,y,z rates
 * @return -1 if i2c err, else 0
 */
int mpu3300_read_raw(mpu3300_gyro_raw_t * gyro_reading);

/**
 * Gyro sleep mode control.
 * @param sleep 0 for on and 1 for sleep.
 * @return
 */
int mpu3300_sleep(uint8_t sleep);

#endif
