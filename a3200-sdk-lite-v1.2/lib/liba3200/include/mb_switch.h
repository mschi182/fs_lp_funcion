#ifndef MB_SWITCH_H_
#define MB_SWITCH_H_

typedef enum {
	MB_SWITCH_GPS = 0,
	MB_SWITCH_WDE = 1,
} mb_switch_e ;

/**
 * Run this before the other functions
 */
void mb_switch_init(void);

/**
 * Turn MB switch on
 * @param mb_switch use ENUM for mb_switch names
 */
void mb_switch_enable(mb_switch_e mb_switch);

/**
 * Turn MB switch off
 * @param mb_switch use ENUM for mb_switch names
 */
void mb_switch_disable(mb_switch_e mb_switch);

#endif /* MB_SWITCH_H_ */
