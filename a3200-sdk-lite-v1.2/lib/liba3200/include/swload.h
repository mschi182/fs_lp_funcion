/**
 * A3200 firmware
 *
 * @author Johan De Claville Christiansen
 * Copyright 2015 GomSpace ApS. All rights reserved.
 */

#ifndef SWLOAD_H_
#define SWLOAD_H_

/**
 * Load software image from file
 * @param path filename
 */
int swload_file(char * path, void * swload_link_addr, uint32_t swload_max_sz);

#endif /* SWLOAD_H_ */
