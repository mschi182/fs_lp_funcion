/**
 * ADC channel driver
 *
 * @author Jesper Wramberg & Mathias Tausen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#ifndef ADC_CHANNELS_H_
#define ADC_CHANNELS_H_

#include <conf_liba3200.h>

#if BOARDREV >= 3
#define ADC_NCHANS		12
#else
#define ADC_NCHANS		3
#endif

#define ADC_REF			2500
#define ADC_MAX_VAL 	(2048 - 1)
#define ADC_TO_MV	 	((float) ADC_REF / (float) ADC_MAX_VAL)

/** 
 * Conversion macro 
 * 0.025 Resistor, 
 * 100V/V gain in curr sensor,
 * 2.5V reference,  
 * 11-bit resolution from 0 to 2.5V
 */
#define ADC_CONVERT_MA(val)		(val * (2.5/2048) / (0.0025)) 

/**
 * Initialise ADC
 * This function must be called before any other function in this file.
 * This will call adc_channels_calibrate, so power switches must be OFF.
 * Also, the adc_channels_ functions should have exclusive access to the
 * ADC hardware, or it will not work.
 */
void adc_channels_init(void);

/**
 * Get a single sample from each channel. This function does a blocking
 * sample on all used adc channels and writes those to the array pointed
 * to by adc_values. This function must only be called from Task context
 * @param adc_values Pointer to short int array of dimension ADC_NCHANS
 */
int adc_channels_sample(short int *adc_values);

#endif /* ADC_CHANNELS_H_ */
