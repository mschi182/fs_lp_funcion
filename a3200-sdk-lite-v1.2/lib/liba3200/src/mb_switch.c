#include <gpio.h>
#include "mb_switch.h"
#include <param/param.h>
#include <a3200_board.h>

#define MB_SWITCH_GPS_PIN AVR32_ADCIN1_PIN
#define MB_SWITCH_WDE_PIN AVR32_ADCIN0_PIN

void mb_switch_init(void) {
	gpio_configure_pin(MB_SWITCH_GPS_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
    gpio_configure_pin(MB_SWITCH_WDE_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
}

void mb_switch_enable(mb_switch_e mb_switch) {
    switch (mb_switch) {
        case MB_SWITCH_GPS:
            gpio_set_pin_low(MB_SWITCH_GPS_PIN);
            gpio_enable_module_pin(AVR32_USART1_TXD_PIN, AVR32_USART1_TXD_FUNCTION);
            param_set_uint8_nocallback(A3200_PWR_GPS, A3200_BOARD, 1);
            break;
        case MB_SWITCH_WDE:
            gpio_set_pin_low(MB_SWITCH_WDE_PIN);
            param_set_uint8_nocallback(A3200_PWR_WDE, A3200_BOARD, 1);
            break;
    }
}

void mb_switch_disable(mb_switch_e mb_switch) {
    switch (mb_switch) {
        case MB_SWITCH_GPS:
            gpio_set_pin_high(MB_SWITCH_GPS_PIN);
            gpio_configure_pin(AVR32_USART1_TXD_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
            param_set_uint8_nocallback(A3200_PWR_GPS, A3200_BOARD, 0);
            break;
        case MB_SWITCH_WDE:
            gpio_set_pin_high(MB_SWITCH_WDE_PIN);
            param_set_uint8_nocallback(A3200_PWR_WDE, A3200_BOARD, 0);
            break;
    }
}
