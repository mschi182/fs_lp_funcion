/*
 * cmd_pwm.c
 *
 *  Created on: Jun 19, 2014
 *      Author: bisgaard
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include <dev/usart.h>
#include <util/console.h>
#include <FreeRTOS.h>
#include <task.h>

#include "gs_pwm.h"

int cmd_pwm_init(struct command_context *ctx) {

	gs_pwm_init();

	return CMD_ERROR_NONE;
}


int cmd_pwm_set(struct command_context *ctx) {
	int channel;
	float duty,freq;

	if (ctx->argc != 4)
		return CMD_ERROR_SYNTAX;

	channel = atoi(ctx->argv[1]);
	duty = atof(ctx->argv[2]);
	freq = atof(ctx->argv[3]);

	if (channel < 0 || channel > 2)
		return CMD_ERROR_SYNTAX;

	//gs_pwm_init();
	gs_pwm_enable(channel);

	printf("Freq requested: %f Hz, achieved: %f Hz\r\n",freq,gs_pwm_set_freq(channel, freq));
	gs_pwm_set_duty(channel, duty);

	return CMD_ERROR_NONE;
}

command_t __sub_command pwm_subcommands[] = {
		{
				.name = "set",
				.help = "Set PWM",
				.usage = "<channel 0,1,2> <dutycycle (-100 to +100)> <frequency (Hz)>",
				.handler = cmd_pwm_set
		},
		{
				.name = "Init",
				.help = "Init PWM",
				.usage = "<>",
				.handler = cmd_pwm_init
		}
};

command_t __sub_command __root_command pwm_commands[] = {
		{
				.name = "pwm",
				.help = "PWM commands",
				.chain = INIT_CHAIN(pwm_subcommands),
		},
};

void cmd_pwm_setup(void) {
	command_register(pwm_commands);
}
