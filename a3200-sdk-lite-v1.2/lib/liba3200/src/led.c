/**
 * NanoCom firmware
 *
 * @author Johan De Claville Christiansen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <gpio.h>
#include "led.h"
#include <conf_liba3200.h>

void led_init(void) {
#if LED_ENABLED
	gpio_configure_pin(LED_PIN_0, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
	gpio_configure_pin(LED_PIN_1, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
#endif
}

void led_on(led_names_t led) {
#if LED_ENABLED
	switch(led) {
		case LED_CPUOK:
			gpio_set_pin_high(LED_PIN_0);
			break;
		case LED_A:
			gpio_set_pin_high(LED_PIN_1);
			break;
	}
#endif
}

void led_off(led_names_t led) {
#if LED_ENABLED
	switch(led) {
		case LED_CPUOK:
			gpio_set_pin_low(LED_PIN_0);
			break;
		case LED_A:
			gpio_set_pin_low(LED_PIN_1);
			break;
	}
#endif
}

void led_toggle(led_names_t led) {
#if LED_ENABLED
	switch(led) {
		case LED_CPUOK:
			gpio_toggle_pin(LED_PIN_0);
			break;
		case LED_A:
			gpio_toggle_pin(LED_PIN_1);
			break;
	}
#endif
}

