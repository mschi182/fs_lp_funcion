#include <FreeRTOS.h>
#include <task.h>
#include <time.h>

#include <csp/csp.h>
#include <csp/csp_cmp.h>
#include <csp/csp_endian.h>

#include <util/log.h>

#include "../client/a3200_board.h"

int do_csp_timesync(uint8_t tsync_node, uint32_t timeout) {
	struct csp_cmp_message msg = {};
	msg.clock.tv_sec = 0;
	msg.clock.tv_nsec = 0;
	if (csp_cmp_clock(tsync_node, timeout, &msg) == CSP_ERR_NONE) {
		csp_timestamp_t now;
		now.tv_sec = csp_ntoh32(msg.clock.tv_sec);
		now.tv_nsec = csp_ntoh32(msg.clock.tv_nsec);
		if (now.tv_sec > 0) {
			/* Set clock and ticks */
			clock_set_time(&now);
			return 1;
		}
	}

	return 0;
}

void task_timesync(void * pvParameters) {

	csp_timestamp_t now;
	TickType_t start = 0;
	while (1) {
		vTaskDelayUntil(&start, 1000);

		/* Get current time */
		clock_get_time(&now);

		/* Time sync if enabled */
		int16_t tsync_node = param_get_int16(A3200_TSYNC_NODE, A3200_BOARD);
		uint16_t tsync_intv = param_get_uint16(A3200_TSYNC_INTV, A3200_BOARD);
		if ((tsync_node > 0) && (tsync_intv > 0)) {
			if ((now.tv_sec % tsync_intv) == 0) {
				do_csp_timesync(tsync_node, 1000);
			}
		}
	}
}
