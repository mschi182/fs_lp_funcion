/**
 * A3200 firmware
 *
 * @author Johan De Claville Christiansen
 * Copyright 2015 GomSpace ApS. All rights reserved.
 */

#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>

#include <FreeRTOS.h>
#include <task.h>

#include <util/log.h>

int swload_file(char * path, void * swload_link_addr, uint32_t swload_max_sz) {

	/* Variables */
	FILE *fd;
	struct stat fno;
	int result;
	unsigned int read, ms;
	portTickType start_time, stop_time;

	/* Open file */
	fd = fopen(path, "r");
	if (fd == NULL) {
		log_warning("File not found %s", path);
		return -1;
	} else {
		log_debug("File open %s", path);
	}

	/* Read file size */
	result = stat(path, &fno);
	if (result != 0) {
		log_debug("Fstat failed on %s", path);
		return -1;
	} else {
		log_debug("File size is %lu", fno.st_size);
	}

	/* Check if file size fits within swload area */
	if (fno.st_size >= swload_max_sz) {
		log_error("File size too large %lu", fno.st_size);
		fclose(fd);
		return -1;
	}

	/* Read file */
	start_time = xTaskGetTickCount();
	read = fread(swload_link_addr, 1, fno.st_size, fd);
	stop_time = xTaskGetTickCount();
	ms = (stop_time - start_time) * (1000/configTICK_RATE_HZ);
	fclose(fd);

	/* Print result */
	if (read != fno.st_size) {
		log_error("Failed to read file");
		return -1;
	} else {
		log_debug("Read result %d, read %u in %u ms (%u KBytes/sec)", result, read, ms, read/ms);
	}

	/* Software pre-loaded, give user 10 seconds to abort (reset and run swload) */
	log_info("Booting image in 10 seconds...");
	vTaskDelay(10 * configTICK_RATE_HZ);

	/* Jump to address */
	log_debug("Jumping to address %p", (void *) swload_link_addr);

	DISABLE_ALL_INTERRUPTS();
	void (*jump)(void) __attribute__((__noreturn__)) = (void *) swload_link_addr;
	jump();

	/* Naah, this will not happen */
	return 0;

}
