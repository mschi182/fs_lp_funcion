/**
 * NanoCom firmware
 *
 * @author Johan De Claville Christiansen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <math.h>

#include <spi.h>
#include <gpio.h>
#include <sysclk.h>
#include <util/log.h>

#include "board.h"

void lm70_init(void) {

	/* Spi options */
	spi_options_t spi_opt = {};
	spi_opt.modfdis = 1;

	/* Init CS (TEMP1 + TEMP2) */
	spi_opt.baudrate = 16000000;
	spi_opt.bits = 16;
	spi_opt.reg = LM70_SPI_CS;
	spi_opt.spi_mode = 0;
	spi_opt.stay_act = 1;
	spi_opt.trans_delay = 0;
	spi_setupChipReg(LM70_SPI, &spi_opt, sysclk_get_peripheral_bus_hz(LM70_SPI));

	/* Setup GPIO for chip select on TEMP1 and TEMP2 */
	gpio_configure_pin(LM70_SPI_CS_TEMP1_PIN, GPIO_DIR_OUTPUT | GPIO_PULL_UP | GPIO_INIT_HIGH);
	gpio_configure_pin(LM70_SPI_CS_TEMP2_PIN, GPIO_DIR_OUTPUT | GPIO_PULL_UP | GPIO_INIT_HIGH);

}

int16_t lm70_read_temp(int sensor) {

	if (sensor < 1 || sensor > 2) {
		log_error("invalid sensor");
		return 0;
	}

	spi_selectChip(LM70_SPI, LM70_SPI_CS);
	if (sensor == 1) {
		gpio_set_pin_low(LM70_SPI_CS_TEMP1_PIN);
	} else {
		gpio_set_pin_low(LM70_SPI_CS_TEMP2_PIN);
	}

	spi_write(LM70_SPI, 0xffff);
	uint16_t retval = 0;
	spi_read(LM70_SPI, &retval);

	if (sensor == 1) {
		gpio_set_pin_high(LM70_SPI_CS_TEMP1_PIN);
	} else {
		gpio_set_pin_high(LM70_SPI_CS_TEMP2_PIN);
	}

	spi_unselectChip(LM70_SPI, LM70_SPI_CS);

	/* Temp in 1/32'th degrees */
	float temp = ((int16_t) retval) >> 2;

	/* Convert into 1/10'th degrees */
	temp = temp / 3.2;

	return round(temp);

}



