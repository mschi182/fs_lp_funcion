/**
 * @author Johan De Claville Christiansen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <stdio.h>
#include <string.h>
#include <conf_a3200.h>

#include <gpio.h>
#include <sysclk.h>
#include <FreeRTOS.h>
#include <task.h>

#include <csp/csp.h>
#include <csp/interfaces/csp_if_kiss.h>
#include <csp/interfaces/csp_if_i2c.h>
#include <csp/interfaces/csp_if_can.h>

#include <dev/usart.h>

#include <util/clock.h>
#include <util/log.h>
#include <util/console.h>

#include <param/param.h>
#include <rparam_server.h>

#include <board_init.h>
#include <a3200_board.h>
#include <pwr_switch.h>
#include <led.h>

static void init_csp(void) {

	/* CSP */
	log_csp_init();
	csp_buffer_init(400, 256 + 16);
	csp_set_hostname("OBC");
	csp_set_model("A3200");
	csp_init(1);

	/* Routes */
	static csp_iface_t csp_if_kiss;
	static csp_kiss_handle_t csp_kiss_handle;
	void kiss_putc_f(char c) {
		usart_putc(USART_CONSOLE, c);
	}
	void kiss_discard_f(char c, void * pxTaskWoken) {
		usart_insert(USART_CONSOLE, c, pxTaskWoken);
	}
	void my_usart_callback(uint8_t * buf, int len, void * pxTaskWoken) {
		csp_kiss_rx(&csp_if_kiss, buf, len, pxTaskWoken);
	}
	csp_kiss_init(&csp_if_kiss, &csp_kiss_handle, kiss_putc_f, kiss_discard_f, "KISS");
	usart_set_callback(USART_CONSOLE, my_usart_callback);

	/* Setup GPIO map for twi connection in stack */
	const gpio_map_t TWIM_GPIO_MAP2 = {
		{AVR32_TWIMS0_TWCK_0_0_PIN, AVR32_TWIMS0_TWCK_0_0_FUNCTION},
		{AVR32_TWIMS0_TWD_0_0_PIN, AVR32_TWIMS0_TWD_0_0_FUNCTION}
	};
	gpio_enable_module(TWIM_GPIO_MAP2, sizeof(TWIM_GPIO_MAP2) / sizeof(TWIM_GPIO_MAP2[0]));
	/* Init csp i2c interface with address 1 and 100 kHz clock */
	csp_i2c_init(1, 0, 400);

	char *ifc = "can0";
	struct csp_can_config conf = {.ifc = ifc};
	conf.bitrate = 1000 * 1000;
	conf.clock_speed = BOARD_OSC0_HZ;
	csp_can_init(CSP_CAN_MASKED, &conf);

	/* Check for stored routing table */
	char * rtable_str = param_get_string(A3200_RTABLE_STR, A3200_BOARD);
	if (rtable_str && (csp_rtable_check(rtable_str) >= 1)) {
		log_info("Route load %s", rtable_str);
		csp_rtable_clear();
		csp_rtable_load(rtable_str);

	/* Otherwise, setup default routing table based on CSP address */
	} else {

		csp_route_set(8, &csp_if_kiss, CSP_NODE_MAC);
		csp_rtable_set(0, 2, &csp_if_i2c, CSP_NODE_MAC);
		csp_rtable_set(6, 5, &csp_if_can, 6);
		csp_rtable_set(0, 0, &csp_if_i2c, 5);

	}

	/* Start the server task which handles all the standard csp commands and
	 * also clears the watch dog every second */
	void server_task(void * param);
	xTaskCreate(server_task, "SRV", 4000, NULL, 2, NULL);
	
	/* Start the router task with priority 3 and 4000 bytes of stack */
	csp_route_start_task(4000, 3);
}

void init_task(void * param) {

	/* Board init */
	board_init();

	/* Can driver is always inited */
	init_can(1);

	/* Mission specific early init */
	extern void hook_init_early(void);
	hook_init_early();

	/* Init CSP, this will also start the server task 
	 * which is responsible for clearing the wdt
	 */
	init_csp();

	/* Init VMEM */
	csp_cmp_set_memcpy((csp_memcpy_fnc_t) vmem_cpy);

	/* Spawn a task for mounting as this can take some time */
	void task_mount(void * param);
	xTaskCreate(task_mount, "MNT", 4000, NULL, 1, NULL);

	/* Console */
	command_init();
	console_set_hostname(CONFIG_HOSTNAME);
	xTaskCreate(debug_console, "CONSOLE", 4000, NULL, 1, NULL);

	/* Start board task */
	void task_board(void * param);
	xTaskCreate(task_board, "BOARD", 4000, NULL, 1, NULL);
	
	/* GSSB */
	pwr_switch_enable(PWR_GSSB2);
	void gssb_cmd_task(void * param);
	xTaskCreate(gssb_cmd_task, "GSSB", 4000, NULL, 2, NULL);

	/* Mission specific init */
	extern void hook_init(void);
	hook_init();

	vTaskDelete(NULL);
}
