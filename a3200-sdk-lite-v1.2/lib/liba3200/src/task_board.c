#include <stdio.h>

#include <FreeRTOS.h>
#include <task.h>

#include <util/log.h>
#include <util/clock.h>
#include <led.h>
#include <adc_channels.h>
#include <lm70.h>
#include <conf_liba3200.h>
#include "../client/a3200_board.h"

void task_board(void * param) {
	TickType_t start = 0;
	while (1) {
		vTaskDelayUntil(&start, 1000);

		/* Sample currents */
		int16_t adc[ADC_NCHANS];
		if (adc_channels_sample(adc) == 0) {
#if BOARDREV >= 3
			param_set_uint16(A3200_CUR_GSSB1, A3200_BOARD, ADC_TO_MV * adc[11] / 0.220 / 100);
			param_set_uint16(A3200_CUR_GSSB2, A3200_BOARD, ADC_TO_MV * adc[9] / 0.220 / 100);
			param_set_uint16(A3200_CUR_FLASH, A3200_BOARD, ADC_TO_MV * adc[10] / 0.220 / 100);
			param_set_uint16(A3200_CUR_PWM, A3200_BOARD, ADC_TO_MV * adc[8] / 0.100 / 100);
			param_set_uint16(A3200_CUR_GPS, A3200_BOARD, ADC_TO_MV * adc[3] / 0.025 / 100);
			param_set_uint16(A3200_CUR_WDE, A3200_BOARD, ADC_TO_MV * adc[2] / 0.025 / 100);
#else
			param_set_uint16(A3200_CUR_GSSB1, A3200_BOARD, ADC_TO_MV * adc[2] / 0.025 / 100);
			param_set_uint16(A3200_CUR_GSSB2, A3200_BOARD, ADC_TO_MV * adc[0] / 0.025 / 100);
			param_set_uint16(A3200_CUR_FLASH, A3200_BOARD, ADC_TO_MV * adc[1] / 0.025 / 100);
#endif
		}

		/* Sample temperatures */
		param_set_int16(A3200_TEMP_A, A3200_BOARD, lm70_read_temp(1));
		param_set_int16(A3200_TEMP_B, A3200_BOARD, lm70_read_temp(2));

		/* Set clock and ticks */
		timestamp_t now;
		clock_get_time(&now);
		param_set_uint32(A3200_CLOCK, A3200_BOARD, now.tv_sec);
		param_set_uint32(A3200_TICKS, A3200_BOARD, xTaskGetTickCount());

	}
}
