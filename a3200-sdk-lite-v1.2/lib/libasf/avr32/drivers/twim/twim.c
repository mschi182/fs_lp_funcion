/*****************************************************************************
 *
 * \file
 *
 * \brief TWIM driver for AVR32 UC3.
 *
 * This file defines a useful set of functions for TWIM on AVR32 devices.
 *
 *****************************************************************************/
/**
 * Copyright (c) 2010-2014 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#include <inttypes.h>
#include <stdio.h>

#include <util/log.h>
LOG_GROUP_SILENT(log_twim, "twim");
#define LOG_DEFAULT log_twim

#include "twim.h"
#include "pdca.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define TWIM_ISR_LEVEL 0

/* Forward declaration */
__attribute__((__naked__)) void twim0_ISR();
__attribute__((__naked__)) void twim1_ISR();
__attribute__((__naked__)) void twim2_ISR();

static struct {
	xSemaphoreHandle lock;
	xSemaphoreHandle isr_wait;
	volatile avr32_twim_t *dev;
	volatile twim_transfer_status_t transfer_status;
	__int_handler irq_handler;
	int irq;
	int pid_rx;
	int pid_tx;
	int pdc_chan_rx;
	int pdc_chan_tx;
} twim[3] = {
	{.dev = &AVR32_TWIM0, .transfer_status = TWI_SUCCESS, .lock = NULL, .irq_handler = twim0_ISR, .irq = AVR32_TWIM0_IRQ, .pid_rx = AVR32_PDCA_PID_TWIM0_RX, .pid_tx = AVR32_PDCA_PID_TWIM0_TX, .pdc_chan_tx = 9, .pdc_chan_rx = 12},
	{.dev = &AVR32_TWIM1, .transfer_status = TWI_SUCCESS, .lock = NULL, .irq_handler = twim1_ISR, .irq = AVR32_TWIM1_IRQ, .pid_rx = AVR32_PDCA_PID_TWIM1_RX, .pid_tx = AVR32_PDCA_PID_TWIM1_TX, .pdc_chan_tx = 10, .pdc_chan_rx = 13},
	{.dev = &AVR32_TWIM2, .transfer_status = TWI_SUCCESS, .lock = NULL, .irq_handler = twim2_ISR, .irq = AVR32_TWIM2_IRQ, .pid_rx = AVR32_PDCA_PID_TWIM2_RX, .pid_tx = AVR32_PDCA_PID_TWIM2_TX, .pdc_chan_tx = 11, .pdc_chan_rx = 14},
};

static void twim_lock(int handler) {
	if (twim[handler].lock == NULL) {
		twim[handler].lock = xSemaphoreCreateMutex();
	}
	if (xSemaphoreTake(twim[handler].lock, 0.1 * configTICK_RATE_HZ) == pdFALSE) {
		log_error("I2C deadlock %u", handler);
	} else {
		log_trace("Lock %p", xTaskGetCurrentTaskHandle());
	}
}

static void twim_unlock(int handler) {
	log_trace("Unlock %p", xTaskGetCurrentTaskHandle());
	if (twim[handler].lock != NULL)
		xSemaphoreGive(twim[handler].lock);
}

__attribute__((__noinline__)) portBASE_TYPE twim_DSR(int handle) {

	portBASE_TYPE xTaskWoken = pdFALSE;

	volatile avr32_twim_t *twim_inst = twim[handle].dev;

	// get masked status register value
	uint32_t status = twim_inst->sr & twim_inst->imr;

	log_debug("M%u P%lx", handle, status);

	/* NACK */
	if (status & AVR32_TWIM_SR_NAK_MASK) {
		log_warning("NACK");
		twim[handle].transfer_status = TWI_RECEIVE_NACK;
		twim_inst->CMDR.valid = 0;
		twim_inst->scr = AVR32_TWIM_SR_NAK_MASK;
	}

	/* ARBITRATION LOST */
	if (status & AVR32_TWIM_SR_ARBLST_MASK) {
		log_warning("ARBLOST");
		twim[handle].transfer_status = TWI_ARBITRATION_LOST;
		twim_inst->CMDR.valid = 0;
		twim_inst->scr = AVR32_TWIM_SR_ARBLST_MASK;
	}

	/* Command Complete */
	if (status & AVR32_TWIM_SR_CCOMP_MASK) {
		twim_inst->scr = AVR32_TWIM_SCR_CCOMP_MASK;
	}

	/* We are idle now */
	if (status & AVR32_TWIM_SR_IDLE_MASK) {
		twim_inst->idr = AVR32_TWIM_SR_IDLE_MASK;
	}

	/* Every interrupt should wake task */
	xSemaphoreGiveFromISR(twim[handle].isr_wait, &xTaskWoken);

	return xTaskWoken;
}

__attribute__((__naked__)) void twim0_ISR() {
	portENTER_SWITCHING_ISR();
	twim_DSR(0);
	portEXIT_SWITCHING_ISR();
}

__attribute__((__naked__)) void twim1_ISR() {
	portENTER_SWITCHING_ISR();
	twim_DSR(1);
	portEXIT_SWITCHING_ISR();
}

__attribute__((__naked__)) void twim2_ISR() {
	portENTER_SWITCHING_ISR();
	twim_DSR(2);
	portEXIT_SWITCHING_ISR();
}

static void twim_busready_wait(int handle) {

	/* Remember when we started waiting */
	portTickType start = xTaskGetTickCount();

	while (1) {

		/* Check if we are idle */
		unsigned long sr = twim[handle].dev->sr;
		if (sr & AVR32_TWIM_SR_IDLE_MASK)
			break;

		/* Check if we have been waiting for too long */
		if (xTaskGetTickCount() > start + (0.1 * configTICK_RATE_HZ)) {
			twim[handle].transfer_status = TWI_TIMEOUT;
			log_error("TWIM BUSFREE TIMEOUT %lx %lx", sr, twim[handle].dev->cmdr);
			twim[handle].dev->cr = AVR32_TWIM_CR_MEN_MASK;
			twim[handle].dev->cr = AVR32_TWIM_CR_SWRST_MASK;
			twim[handle].dev->cr = AVR32_TWIM_CR_MEN_MASK;
			break;
		}

		/* Yield, sleep or somehow do nothing */
		log_warning("TWIM%d not ready %lx", handle, sr);
		vTaskDelay(1);
		//cpu_relax();

	}

}

/**
 * Initialize twi master
 * @param handle twi master handle id 0,1,2
 * @param pba_hz speed of the pba bus
 * @param speed speed of the i2c bus in khz
 * @return 0 if ok -1 if otherwise
 */
int twim_init(int handle, int pba_hz, int speed) {

	/* Reset the hardware */
	twim[handle].dev->idr = ~0UL;
	twim[handle].dev->scr = ~0UL;
	twim[handle].dev->cr = AVR32_TWIM_CR_MEN_MASK;
	twim[handle].dev->cr = AVR32_TWIM_CR_SWRST_MASK;
	twim[handle].dev->cr = AVR32_TWIM_CR_MEN_MASK;
	twim[handle].dev->scr = ~0UL;

	/* Select the speed */
	uint32_t f_prescaled;
	uint8_t cwgr_exp = 0;
	f_prescaled = (pba_hz / speed / 2);

	/* f_prescaled must fit in 8 bits, cwgr_exp must fit in 3 bits */
	while ((f_prescaled > 0xFF) && (cwgr_exp <= 0x7)) {
		cwgr_exp++;
		f_prescaled /= 2;
	}

	/* Check if speed could be obtained */
	if (cwgr_exp > 0x7) {
		log_error("Invalid I2C speed");
		return -1;
	}

	/* set clock waveform generator register */
	twim[handle].dev->cwgr = ((f_prescaled * 1/2) << AVR32_TWIM_CWGR_LOW_OFFSET)
			| ((f_prescaled * 1/2) << AVR32_TWIM_CWGR_HIGH_OFFSET)
			| (cwgr_exp << AVR32_TWIM_CWGR_EXP_OFFSET)
			| ((f_prescaled >> 3) << AVR32_TWIM_CWGR_DATA_OFFSET)
			| (0xff << AVR32_TWIM_CWGR_STASTO_OFFSET);

#if 0
	int f_twim = pba_hz / pow(2, cwgr_exp + 1);
	log_debug("F_TWIM %d %p", f_twim, twim);
	log_debug("t_low %u, t 1/%u Hz, %f us", twim->CWGR.low, f_twim / twim->CWGR.low, 1000000.0 / (f_twim / twim->CWGR.low));
	log_debug("t_high %u, t 1/%u Hz, %f us", twim->CWGR.high, f_twim / twim->CWGR.high, 1000000.0 / (f_twim / twim->CWGR.high));
	log_debug("t_stasto %u, t 1/%u Hz, %f us", twim->CWGR.stasto, f_twim / twim->CWGR.stasto, 1000000.0 / (f_twim / twim->CWGR.stasto));
	log_debug("t_data %u, t 1/%u Hz, %f us", twim->CWGR.data, f_twim / twim->CWGR.data, 1000000.0 / (f_twim / twim->CWGR.data));
#endif

	/* Create synchronization semaphore */
	twim[handle].isr_wait = xSemaphoreCreateBinary();

	/* Register ISR */
	irq_register_handler(twim[handle].irq_handler, twim[handle].irq, TWIM_ISR_LEVEL);

	/* TX PDC */
	pdca_channel_options_t pdcopt_tx = { .pid = twim[handle].pid_tx, .transfer_size = PDCA_TRANSFER_SIZE_BYTE, };
	pdca_init_channel(twim[handle].pdc_chan_tx, &pdcopt_tx);

	/* RX PDC */
	pdca_channel_options_t pdcopt_rx = { .pid = twim[handle].pid_rx, .transfer_size = PDCA_TRANSFER_SIZE_BYTE, };
	pdca_init_channel(twim[handle].pdc_chan_rx, &pdcopt_rx);

	return 0;
}

/**
 * Perform a twi master transfer using the PDC
 * @param handle twi master handle id 0,1,2
 * @param tx pointer to tx object
 * @param rx pointer to rx object or NULL if no RX is desired
 * @return 0 if ok, < 0 otherwise
 */
int twim_pdc_transfer(int handle, volatile twim_transfer_t *tx, volatile twim_transfer_t *rx) {

	twim_lock(handle);

	if (rx != NULL) {
		log_info("Chained %u tx %"PRIu32", rx %"PRIu32, handle, tx->length, rx->length);
	} else {
		log_info("Write %u tx %"PRIu32, handle, tx->length);
	}

	twim_busready_wait(handle);

	portENTER_CRITICAL();

    // Clear the interrupt flags
	twim[handle].dev->idr = ~0UL;
	// Clear the status flags
	twim[handle].dev->scr = ~0UL;
	// Initialize bus transfer status
	twim[handle].transfer_status = TWI_SUCCESS;

	xSemaphoreTake(twim[handle].isr_wait, 0);

	/* Overwrite THR (a byte may be stuck there) */
	twim[handle].dev->THR.txdata = tx->buffer[0];

	/* Chained transfer */
	if (rx != NULL) {
		twim[handle].dev->cmdr = (tx->chip << AVR32_TWIM_CMDR_SADR_OFFSET)
				| (tx->length << AVR32_TWIM_CMDR_NBYTES_OFFSET)
				| (AVR32_TWIM_CMDR_VALID_MASK)
				| (AVR32_TWIM_CMDR_START_MASK);

		twim[handle].dev->ncmdr = (rx->chip << AVR32_TWIM_CMDR_SADR_OFFSET)
				| (rx->length << AVR32_TWIM_CMDR_NBYTES_OFFSET)
				| (AVR32_TWIM_CMDR_VALID_MASK)
				| (AVR32_TWIM_CMDR_START_MASK)
				| (AVR32_TWIM_CMDR_STOP_MASK)
				| (AVR32_TWIM_CMDR_READ_MASK);

	/* Single transfer */
	} else {
		twim[handle].dev->cmdr = (tx->chip << AVR32_TWIM_CMDR_SADR_OFFSET)
				| (tx->length << AVR32_TWIM_CMDR_NBYTES_OFFSET)
				| (AVR32_TWIM_CMDR_VALID_MASK)
				| (AVR32_TWIM_CMDR_START_MASK)
				| (AVR32_TWIM_CMDR_STOP_MASK);
	}

	twim[handle].dev->ier = AVR32_TWIM_IER_NAK_MASK | AVR32_TWIM_IER_ARBLST_MASK | AVR32_TWIM_IER_IDLE_MASK;
	pdca_load_channel(twim[handle].pdc_chan_tx, &tx->buffer[1], tx->length - 1);
	pdca_enable(twim[handle].pdc_chan_tx);

	if (rx != NULL) {
		pdca_load_channel(twim[handle].pdc_chan_rx, rx->buffer, rx->length);
		pdca_enable(twim[handle].pdc_chan_rx);
	}

	portEXIT_CRITICAL();

	/* Wait for hardware to complete */
	if (xSemaphoreTake(twim[handle].isr_wait, 1 * configTICK_RATE_HZ) == pdFALSE) {
		twim[handle].transfer_status = TWI_TIMEOUT;
		log_error("TWIM TIMEOUT");
	}

	pdca_disable(twim[handle].pdc_chan_tx);
	pdca_disable(twim[handle].pdc_chan_rx);

	twim_unlock(handle);
	return twim[handle].transfer_status;
}
