#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include <wdt.h>
#include <avr32_reset_cause.h>

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed portCHAR *pcTaskName ) {

	printf("STACK OVERFLOW!\r\n");
	printf("In task %p name: %s\r\n", pxTask, pcTaskName);
	wdt_clear();
	reset_do_soft_reset();

}
