#include <dev/usart.h>

int _read_console(int file, char * ptr, int len) {
	int nChars = 0;

	for (; len > 0; --len) {
		char c = usart_stdio_getchar();

		*ptr++ = c;
		++nChars;
	}

	return nChars;
}
