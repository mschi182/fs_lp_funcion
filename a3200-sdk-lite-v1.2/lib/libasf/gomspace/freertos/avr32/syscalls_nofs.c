#include <sys/stat.h>
#include <sys/types.h>
#include <sys/times.h>
#include <errno.h>
#undef errno
extern int errno;

int _fstat(int file, struct stat *st) {
	return 0;
}

int _isatty(int file) {
	return 1;
}

int _link(char *old, char *new) {
	errno = EMLINK;
	return -1;
}

off_t _lseek(int file, off_t ptr, int dir) {
	return 0;
}

int _close(int file) {
	return -1;
}

int _open(const char *name, int flags, int mode) {
	return -1;
}

int _stat(const char *file, struct stat *st) {
	st->st_mode = S_IFCHR;
	return 0;
}

int _unlink(char *name) {
	errno = ENOENT;
	return -1;
}

int fsync(int fd) {
	return -1;
}

/* Use local functions for read/write */
extern int _read_console(int file, char * ptr, int len);
extern int _write_console(int file, const char * ptr, int len);

int _read(int file, char *ptr, int len) {
	return _read_console(file, ptr, len);
}

int _write(int file, const char *ptr, int len) {
	return _write_console(file, ptr, len);
}
