#include <unistd.h>
#include <stdint.h>
#include <dev/usart.h>

/**
 * Low-level write command.
 * When newlib buffer is full or fflush is called, this will output
 * data to correct location.
 * 1 and 2 is stdout and stderr which goes to usart
 * 3 is framebuffer
 */
int _write_console(int file, const char * ptr, size_t len) {
	int nChars = 0;

	for (; len != 0; --len) {
		usart_stdio_putchar(*ptr++);
		++nChars;
	}

	return nChars;
}
