/*
 * usart_wrapper.c
 *
 *  Created on: 21-08-2009
 *      Author: Administrator
 */

// Standard Includes
#include <stdint.h>
#include <avr32/io.h>

// FreeRTOS
#include <FreeRTOS.h>
#include <queue.h>
#include <task.h>

// Implement the public USART interface
#include <dev/usart.h>

// Using local driver functionality
#include "usart.h"
#include "gpio.h"
#include "pdca.h"
#include "intc.h"

int usart_stdio_id = -1;

#define USART_RX_PDC_BUF_SIZE	512
#define USART_NO_PDC 0

struct usart_struct {
		uint8_t usart_rx_buf[2][USART_RX_PDC_BUF_SIZE];
		volatile avr32_usart_t * addr;
		xQueueHandle usart_rxqueue;
		usart_callback_t rx_callback;
		volatile uint8_t current_buf;
		int pdc_channel_rx;
		int pdc_pid_rx;
		__int_handler pdc_isr;
		__int_handler isr;
		int irq;
};

struct usart_struct * usart[AVR32_USART_NUM] = {0};

__attribute__((__noinline__)) portBASE_TYPE usart_DSR(struct usart_struct * usartp) {
	static portBASE_TYPE xTaskWoken;
	xTaskWoken = pdFALSE;
	if (usartp->addr->CSR.usart_mode.rxrdy) {
		char c = (char) usartp->addr->rhr;
		xQueueSendFromISR(usartp->usart_rxqueue, &c, &xTaskWoken);
	}
	return xTaskWoken;
}

__attribute__((__naked__)) void usart0_ISR() {
	portENTER_SWITCHING_ISR();
	usart_DSR(usart[0]);
	portEXIT_SWITCHING_ISR();
}

__attribute__((__naked__)) void usart1_ISR() {
	portENTER_SWITCHING_ISR();
	usart_DSR(usart[1]);
	portEXIT_SWITCHING_ISR();
}

#ifdef AVR32_USART2_ADDRESS
__attribute__((__naked__)) void usart2_ISR() {
	portENTER_SWITCHING_ISR();
	usart_DSR(usart[2]);
	portEXIT_SWITCHING_ISR();
}
#endif

#ifdef AVR32_USART3_ADDRESS
__attribute__((__naked__)) void usart3_ISR() {
	portENTER_SWITCHING_ISR();
	usart_DSR(usart[3]);
	portEXIT_SWITCHING_ISR();
}
#endif

#ifdef AVR32_USART4_ADDRESS
__attribute__((__naked__)) void usart4_ISR() {
	portENTER_SWITCHING_ISR();
	usart_DSR(usart[4]);
	portEXIT_SWITCHING_ISR();
}
#endif

__attribute__((__noinline__)) portBASE_TYPE usart_pdc_DSR(struct usart_struct * usartp) {
	static portBASE_TYPE xTaskWoken;
	xTaskWoken = pdFALSE;

	static int rx_size = 0;
	static uint8_t i;

	/* If no previous RX size was set, we received one byte */
	if (rx_size == 0)
		rx_size = 1;

	/* Process the incoming data (rx_size) */
	if(usartp->rx_callback != NULL)
		usartp->rx_callback((uint8_t *) usartp->usart_rx_buf[usartp->current_buf & 1], rx_size, &xTaskWoken);
	else {
		for(i = 0; i < rx_size; i++)
			xQueueSendFromISR(usartp->usart_rxqueue, &usartp->usart_rx_buf[usartp->current_buf & 1][i], &xTaskWoken);
	}

	/* We now set the reload size to full */
	AVR32_PDCA.channel[usartp->pdc_channel_rx].marr = (unsigned long) usartp->usart_rx_buf[usartp->current_buf & 1];
	AVR32_PDCA.channel[usartp->pdc_channel_rx].tcrr = USART_RX_PDC_BUF_SIZE;
	usartp->current_buf++;

	/* During the processing the main-buffer might be used, so readout the size and save it */
	pdca_disable(usartp->pdc_channel_rx);
	rx_size = USART_RX_PDC_BUF_SIZE - AVR32_PDCA.channel[usartp->pdc_channel_rx].tcr;

	/* If it was used, we would like to return to the interrupt */
	if (rx_size > 0) {
		AVR32_PDCA.channel[usartp->pdc_channel_rx].tcr = 0;

	/* If it wasn't used, we would like to interrupt after next byte */
	} else {
		AVR32_PDCA.channel[usartp->pdc_channel_rx].tcr = 1;
	}
	pdca_enable(usartp->pdc_channel_rx);

	return xTaskWoken;
}

__attribute__((__naked__)) void usart_pdc0_ISR() {
	portENTER_SWITCHING_ISR();
	usart_pdc_DSR(usart[0]);
	portEXIT_SWITCHING_ISR();
}

__attribute__((__naked__)) void usart_pdc1_ISR() {
	portENTER_SWITCHING_ISR();
	usart_pdc_DSR(usart[1]);
	portEXIT_SWITCHING_ISR();
}

#ifdef AVR32_USART2_ADDRESS
__attribute__((__naked__)) void usart_pdc2_ISR() {
	portENTER_SWITCHING_ISR();
	usart_pdc_DSR(usart[2]);
	portEXIT_SWITCHING_ISR();
}
#endif

#ifdef AVR32_USART3_ADDRESS
__attribute__((__naked__)) void usart_pdc3_ISR() {
	portENTER_SWITCHING_ISR();
	usart_pdc_DSR(usart[3]);
	portEXIT_SWITCHING_ISR();
}
#endif

#ifdef AVR32_USART4_ADDRESS
__attribute__((__naked__)) void usart_pdc4_ISR() {
	portENTER_SWITCHING_ISR();
	usart_pdc_DSR(usart[4]);
	portEXIT_SWITCHING_ISR();
}
#endif

/**
 * Wrapper for the ATMEL USART driver init function
 * @param fcpu
 * @param usart_baud
 */
void usart_init(int handle, uint32_t fcpu, uint32_t usart_baud) {

	/* First initialisation */
	if (usart[handle] == NULL) {

		/* Allocate struct (the UC3B does not have cache, so alignment does not matter) */
		usart[handle] = pvPortMalloc(sizeof(struct usart_struct));
		if (usart[handle] == NULL)
			return;

		/* Create Queue */
		usart[handle]->usart_rxqueue = xQueueCreate(USART_RX_PDC_BUF_SIZE, sizeof(char));
		if (usart[handle]->usart_rxqueue == NULL) {
			vPortFree(usart[handle]);
			usart[handle] = NULL;
			return;
		}

		/* Physical addresses */
		switch(handle) {
			default:
			case 0:
				usart[handle]->addr = &AVR32_USART0;
				usart[handle]->pdc_channel_rx = 5;
				usart[handle]->pdc_pid_rx = AVR32_PDCA_PID_USART0_RX;
				usart[handle]->pdc_isr = &usart_pdc0_ISR;
				usart[handle]->isr = &usart0_ISR;
				usart[handle]->irq = AVR32_USART0_IRQ;
				break;
			case 1:
				usart[handle]->addr = &AVR32_USART1;
				usart[handle]->pdc_channel_rx = 6;
				usart[handle]->pdc_pid_rx = AVR32_PDCA_PID_USART1_RX;
				usart[handle]->pdc_isr = &usart_pdc1_ISR;
				usart[handle]->isr = &usart1_ISR;
				usart[handle]->irq = AVR32_USART1_IRQ;
				break;
#ifdef AVR32_USART2_ADDRESS
			case 2:
				usart[handle]->addr = &AVR32_USART2;
				usart[handle]->pdc_channel_rx = 2;
				usart[handle]->pdc_pid_rx = AVR32_PDCA_PID_USART2_RX;
				usart[handle]->pdc_isr = &usart_pdc2_ISR;
				usart[handle]->isr = &usart2_ISR;
				usart[handle]->irq = AVR32_USART2_IRQ;
				break;
#endif
#ifdef AVR32_USART3_ADDRESS
			case 3:
				usart[handle]->addr = &AVR32_USART3;
				usart[handle]->pdc_channel_rx = 8;
				usart[handle]->pdc_pid_rx = AVR32_PDCA_PID_USART3_RX;
				usart[handle]->pdc_isr = &usart_pdc3_ISR;
				usart[handle]->isr = &usart3_ISR;
				usart[handle]->irq = AVR32_USART3_IRQ;
				break;
#endif
#ifdef AVR32_USART4_ADDRESS
			case 4:
				usart[handle]->addr = &AVR32_USART4;
				usart[handle]->pdc_channel_rx = 3;
				usart[handle]->pdc_pid_rx = AVR32_PDCA_PID_USART4_RX;
				usart[handle]->pdc_isr = &usart_pdc4_ISR;
				usart[handle]->isr = &usart4_ISR;
				usart[handle]->irq = AVR32_USART4_IRQ;
				break;
#endif

		}

		/* Init other stuff */
		usart[handle]->current_buf = 0;
		usart[handle]->rx_callback = NULL;

	}

	/* UART channel setup */
	usart_reset(usart[handle]->addr);
	const usart_options_t usart_options = { usart_baud, 8, USART_NO_PARITY,USART_1_STOPBIT, USART_NORMAL_CHMODE};
	usart_init_rs232(usart[handle]->addr, &usart_options, fcpu);

#if USART_NO_PDC

	/* Enable Interrupt */
	INTC_register_interrupt(usart[handle]->isr, usart[handle]->irq, AVR32_INTC_INT0);
	usart[handle]->addr->IER.usart_mode.rxrdy = 1;

#else

	/* Disable PDC/ISR */
	pdca_disable(usart[handle]->pdc_channel_rx);
	pdca_disable_interrupt_reload_counter_zero(usart[handle]->pdc_channel_rx);

	/* Enable PDC */
	pdca_channel_options_t pdc_channel_rx = {};
	pdc_channel_rx.addr = usart[handle]->usart_rx_buf[usart[handle]->current_buf];
	pdc_channel_rx.size = 1;
	pdc_channel_rx.r_addr = usart[handle]->usart_rx_buf[(usart[handle]->current_buf+1) & 0x01];
	pdc_channel_rx.r_size = USART_RX_PDC_BUF_SIZE;
	pdc_channel_rx.pid = usart[handle]->pdc_pid_rx;
	pdc_channel_rx.transfer_size = PDCA_TRANSFER_SIZE_BYTE;
	pdca_init_channel(usart[handle]->pdc_channel_rx, &pdc_channel_rx);

	/* Enable ISR */
	INTC_register_interrupt(usart[handle]->pdc_isr, AVR32_PDCA_IRQ_0 + usart[handle]->pdc_channel_rx, AVR32_INTC_INT0);
	pdca_enable_interrupt_reload_counter_zero(usart[handle]->pdc_channel_rx);
	pdca_enable(usart[handle]->pdc_channel_rx);


#endif

}

void usart_set_callback(int handle, usart_callback_t callback) {
	usart[handle]->rx_callback = callback;
}

void usart_insert(int handle, char c, void * pxTaskWoken) {
	if (pxTaskWoken == NULL)
		xQueueSendToBack(usart[handle]->usart_rxqueue, &c, 0);
	else
		xQueueSendToBackFromISR(usart[handle]->usart_rxqueue, &c, pxTaskWoken);
}

/**
 * Wrapper for ATMEL USART putchar function
 * @param c
 */
void usart_putc(int handle, char c) {
	if (usart[handle] == NULL)
		return;
	usart_putchar(usart[handle]->addr, c);
}

/**
 * Return next char in queue
 */
char usart_getc(int handle) {
	if (usart[handle] == NULL)
		return 0;
	char c;
	xQueueReceive(usart[handle]->usart_rxqueue, &c, portMAX_DELAY);
	return c;
}

char usart_getc_nblock(int handle) {

	unsigned char c =0;
	xQueueReceive(usart[handle]->usart_rxqueue, &c, 0);
	return c;

}

void usart_putstr(int handle, char *buf, int len) {
	int i;
	for (i = 0; i < len; i++)
		usart_putc(handle, buf[i]);
}

int usart_messages_waiting(int handle) {
	return uxQueueMessagesWaiting(usart[handle]->usart_rxqueue);
}
