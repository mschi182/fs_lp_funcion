/*
 * cpu.c
 *
 *  Created on: 22-08-2009
 *      Author: Administrator
 */

#include <stdio.h>
#include <dev/cpu.h>
#include "compiler.h"
#include "wdt.h"
#include "avr32_reset_cause.h"

void cpu_reset(void) {
	wdt_clear();
	reset_do_soft_reset();
}
