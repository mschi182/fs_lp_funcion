#include <stdio.h>

#include <avr32/io.h>
#include <pdca.h>
#include <twim.h>
#include <sysclk.h>

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include <util/log.h>
#include <util/error.h>
#include <util/csp_buffer.h>

#include <dev/i2c.h>

static struct twi_handle_s {
	volatile avr32_twis_t *twis;
	volatile avr32_twim_t *twim;
	uint32_t last_tx;
	xQueueHandle tx_queue;
	xQueueHandle rx_queue;
	i2c_frame_t * rx_frame;
	i2c_callback_t rx_callback;
} twi_device[3] = {};

LOG_GROUP_SILENT(log_twis, "i2c");
#define LOG_DEFAULT log_twis

//! Define all error conditions
#define AVR32_TWIS_SR_ERROR_MASK (AVR32_TWIS_SR_BUSERR_MASK \
		| AVR32_TWIS_SR_SMBPECERR_MASK                      \
		| AVR32_TWIS_SR_SMBTOUT_MASK                        \
		| AVR32_TWIS_SR_ORUN_MASK                           \
		| AVR32_TWIS_SR_URUN_MASK)

#define PDC_CHAN 0
#define PDC_SIZE 256

//! Pointer to the TWIS instance physical address
static volatile avr32_twis_t *twis_inst_slave;

static i2c_frame_t * frame;
static i2c_callback_t rx_callback = NULL;

__attribute__((__noinline__)) portBASE_TYPE i2c_DSR(void) {

	/* FreeRTOS context switch */
	portBASE_TYPE xTaskWoken = pdFALSE;

	/* Get status and interrupt mask register values */
	unsigned long status  = twis_inst_slave->sr;

	log_debug("S%lx", status);

	/* Check for bus error */
	if (status & AVR32_TWIS_SR_ERROR_MASK) {
		log_warning("SLAVE BUS ERROR");
		twis_inst_slave->scr = AVR32_TWIS_SR_ERROR_MASK;
		if (frame != NULL)
			pdca_load_channel(PDC_CHAN, frame->data, PDC_SIZE);
	}

	/* First byte */
	if (status & AVR32_TWIS_SR_RXRDY_MASK) {

		/* Allocate new frame */
		if (frame == NULL)
			frame = csp_buffer_get_isr(PDC_SIZE);

		/* Setup PDC */
		if (frame != NULL) {
			pdca_load_channel(PDC_CHAN, frame->data, PDC_SIZE);
			pdca_enable(PDC_CHAN);
			twis_inst_slave->idr = AVR32_TWIS_IDR_RXRDY_MASK;
			twis_inst_slave->scr = AVR32_TWIS_IDR_RXRDY_MASK;

		/* Out of memory, dump byte */
		} else {
			(void) twis_inst_slave->rhr;
		}

	}

	/* Check if the transmission complete or repeated start flags raised */
	if (status & (AVR32_TWIS_SR_TCOMP_MASK | AVR32_TWIS_SR_REP_MASK)) {

		pdca_disable(PDC_CHAN);

		/* Clear spurious bytes received between stop and start */
		while(twis_inst_slave->sr & AVR32_TWIS_IER_RXRDY_MASK) {
			log_error("Spurious data %"PRIx32, twis_inst_slave->rhr);
		}

		/* Now re-enable first byte ISR */
		twis_inst_slave->ier = AVR32_TWIS_IER_RXRDY_MASK;

		/* Deliver frame to I2C interface */
		if (frame != NULL) {
			frame->len = PDC_SIZE - pdca_get_load_size(PDC_CHAN);
			if (rx_callback != NULL) {
				(*rx_callback)(frame, &xTaskWoken);
			} else {
				log_error("Invalid rx callback function %p", rx_callback);
			}
			frame = NULL;
		}

		/* Clear transmit complete and repeated start flags */
		twis_inst_slave->scr = AVR32_TWIS_SCR_TCOMP_MASK | AVR32_TWIS_SCR_REP_MASK;

	}

	return xTaskWoken;

}

__attribute__((__naked__)) void i2c_ISR() {
	portENTER_SWITCHING_ISR();
	i2c_DSR();
	portEXIT_SWITCHING_ISR();
}

static int twis_init(volatile avr32_twis_t *twis, uint32_t pba_hz, uint32_t speed, uint8_t chip) {

	twis->idr = ~0UL;
	twis->scr = ~0UL;
	twis->cr = AVR32_TWIS_CR_SWRST_MASK;
	twis->scr = ~0UL;

	const pdca_channel_options_t pdcopt = {
		.pid = AVR32_PDCA_PID_TWIS0_RX,
		.transfer_size = PDCA_TRANSFER_SIZE_BYTE,
	};
	pdca_init_channel(PDC_CHAN, &pdcopt);

	irq_register_handler(i2c_ISR, AVR32_TWIS0_IRQ, 0);

	twis_inst_slave = twis;

	// Enable the TWI Slave Module and allow for clock stretching
	twis->cr = AVR32_TWIS_CR_SEN_MASK | AVR32_TWIS_CR_SMATCH_MASK | AVR32_TWIS_CR_STREN_MASK;

	/* Clock stretch after slave address match */
	//twis->cr |= AVR32_TWIS_CR_SOAM_MASK;

	// Set slave address
	twis->cr |= (chip << AVR32_TWIS_CR_ADR_OFFSET);

	// Calculate the wait time from clk falling edge to
	// let the slave control the bus
	uint8_t setup_time = (pba_hz / speed) / 32;
	twis->tr = (setup_time << AVR32_TWIS_TR_SUDAT_OFFSET);

	/* Start the show */
	twis->ier = AVR32_TWIS_SR_ERROR_MASK | AVR32_TWIS_SR_TCOMP_MASK | AVR32_TWIS_SR_REP_MASK | AVR32_TWIS_SR_RXRDY_MASK;

	return 0;
}

int i2c_init_master(int handle, uint8_t addr, uint16_t speed) {

	/** Todo: How to map this efficiently using defines instead? */
	if (handle == 0) {
		twi_device[handle].twim = &AVR32_TWIM0;
	} else if (handle == 1) {
		twi_device[handle].twim = &AVR32_TWIM1;
	} else if (handle == 2) {
		twi_device[handle].twim = &AVR32_TWIM2;
	}

	/* Init HW */
	return twim_init(handle, sysclk_get_peripheral_bus_hz(twi_device[handle].twim), speed * 1000);

}

int i2c_init(int handle, int mode, uint8_t addr, uint16_t speed, int queue_len_tx, int queue_len_rx, i2c_callback_t callback) {

	if (handle > AVR32_TWIS_NUM || handle > 3)
		return E_NO_DEVICE;

	/** Todo: How to map this efficiently using defines instead? */
	if (handle == 0) {
		twi_device[handle].twis = &AVR32_TWIS0;
		twi_device[handle].twim = &AVR32_TWIM0;
	} else if (handle == 1) {
		twi_device[handle].twis = &AVR32_TWIS1;
		twi_device[handle].twim = &AVR32_TWIM1;
	} else if (handle == 2) {
		twi_device[handle].twis = &AVR32_TWIS2;
		twi_device[handle].twim = &AVR32_TWIM2;
	}

	/* Initialise message queues */
	twi_device[handle].tx_queue = xQueueCreate(queue_len_tx, sizeof(i2c_frame_t *));
	if (twi_device[handle].tx_queue == 0)
		return E_NO_BUFFER;

	/* Register either callback for create RX queue */
	if (callback != NULL) {
		twi_device[handle].rx_callback = callback;
		rx_callback = callback;
	} else {
		twi_device[handle].rx_queue = xQueueCreate(queue_len_rx, sizeof(i2c_frame_t *));
		if (twi_device[handle].rx_queue == 0)
			return E_NO_BUFFER;
	}

	/* Slave */
	twis_init(twi_device[handle].twis, sysclk_get_peripheral_bus_hz(twi_device[handle].twis), speed * 1000, addr);

	/* Master */
	twim_init(handle, sysclk_get_peripheral_bus_hz(twi_device[handle].twim), speed * 1000);

	return E_NO_ERR;

}

int i2c_send(int handle, i2c_frame_t * frame, uint16_t timeout) {

	/* TX */
	twim_transfer_t tx;
	tx.chip = frame->dest;
	tx.buffer = frame->data;
	tx.length = frame->len;

	/* We decide to never fail an I2C transmit:
	 * The userspace cannot usually handle a failed TX and it would cause more trouble, than just
	 * deleting the frame and be done with it. */
	twim_pdc_transfer(handle, &tx, NULL);
#if 0
	if (twim_pdc_transfer(handle, &tx, NULL) < 0)
		return E_GARBLED_BUFFER;
#endif

	csp_buffer_free(frame);
	return E_NO_ERR;

}

int i2c_receive(int handle, i2c_frame_t ** frame, uint16_t timeout) {
	return E_NO_DEVICE;
}

int i2c_master_transaction(int handle, uint8_t addr, void * txbuf, size_t txlen, void * rxbuf, size_t rxlen, uint16_t timeout) {

	/* Check if we have anything to do */
	if (txlen == 0) {
		log_error("FIXME: I2C read is not implemented");
		return E_FAIL;
	}

	/* TX */
	twim_transfer_t tx;
	tx.chip = addr;
	tx.buffer = txbuf;
	tx.length = txlen;

	/* RX */
	twim_transfer_t rx;
	rx.chip = addr;
	rx.buffer = rxbuf;
	rx.length = rxlen;

	/* Figure out if we wish to do RX */
	twim_transfer_t *rxptr;
	if (rxlen > 0) {
		rxptr = &rx;
	} else {
		rxptr = NULL;
	}

	if (twim_pdc_transfer(handle, &tx, rxptr) < 0)
		return E_FAIL;

	return E_NO_ERR;

}
