#!/bin/sh
rm -rv `find ./ -iname "iar"`
rm -rv applications/
rm -rv boards/security_xplained/
rm -rv boards/sensors_xplained/
rm -rv boards/user_board/
rm -rv components/crypto/
rm -rv components/display/
rm -rv components/sensor/
rm -rv components/touch/
rm -rv components/wireless/
rm -rv services/usb/
